/*
 * Copyright (c) 2014 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Andreas Sandberg
 */

#include "dev/virtio/block.hh"

#include "debug/VIOBlock.hh"
#include "params/VirtIOBlock.hh"
#include "sim/system.hh"

VirtIOBlock::VirtIOBlock(Params *params)
    : VirtIODeviceBase(params, ID_BLOCK, sizeof(Config), 0),
      qRequests(params->system->physProxy, params->queueSize, *this),
      image(*params->image), ssd_enable(params->SSD_enable),
      vioResponseEvent(this), schPrintEvent(this)
{
    registerQueue(qRequests);

    config.capacity = image.size();
    ssd_latency = 0;

    hil = new HIL(0, ssd_enable, params->SSDConfig);
    schedule(schPrintEvent, 50000000000);
}



void
VirtIOBlock::statsUpdate()
{

  if (ssd_enable) {
    hil->get_parameter(PAL_LAYER, SSD_Stats);
    updatePALStats();
    hil->get_parameter(FTL_HOST_LAYER, SSD_Stats);
    updateFTLStats();
    hil->get_parameter(FTL_PAL_LAYER, SSD_Stats);
    updateFTL_PAL_Stats();
    hil->get_parameter(FTL_MAP_LAYER, SSD_Stats);
    updateFTL_MAP_Stats();
  } else {
    hil->get_parameter(HDD_LAYER, SSD_Stats);
    updateHDDStats();
  }
}



void
VirtIOBlock::updateHDDStats()
{
    HDD_CAP_RD_AVG = SSD_Stats.statistics[RD][CAPACITY][AVG];
    HDD_CAP_RD_MIN = SSD_Stats.statistics[RD][CAPACITY][MIN];
    HDD_CAP_RD_MAX = SSD_Stats.statistics[RD][CAPACITY][MAX];
    HDD_CAP_RD_TOT = SSD_Stats.statistics[RD][CAPACITY][TOT];
    HDD_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    HDD_CAP_WR_AVG = SSD_Stats.statistics[WR][CAPACITY][AVG];
    HDD_CAP_WR_MIN = SSD_Stats.statistics[WR][CAPACITY][MIN];
    HDD_CAP_WR_MAX = SSD_Stats.statistics[WR][CAPACITY][MAX];
    HDD_CAP_WR_TOT = SSD_Stats.statistics[WR][CAPACITY][TOT];
    HDD_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    HDD_CAP_TOT_AVG = SSD_Stats.statistics[ALL][CAPACITY][AVG];
    HDD_CAP_TOT_MIN = SSD_Stats.statistics[ALL][CAPACITY][MIN];
    HDD_CAP_TOT_MAX = SSD_Stats.statistics[ALL][CAPACITY][MAX];
    HDD_CAP_TOT_TOT = SSD_Stats.statistics[ALL][CAPACITY][TOT];
    HDD_CAP_TOT_COUNT = SSD_Stats.statistics[ALL][CAPACITY][COUNT];
    HDD_BW_RD_AVG = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    HDD_BW_RD_MIN = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    HDD_BW_RD_MAX = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    HDD_BW_RD_AVG_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][AVG];
    HDD_BW_RD_MIN_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MIN];
    HDD_BW_RD_MAX_WIDLE = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MAX];
    HDD_BW_RD_AVG_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][AVG];
    HDD_BW_RD_MIN_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MIN];
    HDD_BW_RD_MAX_ONLY = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MAX];
    HDD_BW_WR_AVG = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    HDD_BW_WR_MIN = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    HDD_BW_WR_MAX = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    HDD_BW_WR_AVG_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][AVG];
    HDD_BW_WR_MIN_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MIN];
    HDD_BW_WR_MAX_WIDLE = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MAX];
    HDD_BW_WR_AVG_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][AVG];
    HDD_BW_WR_MIN_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MIN];
    HDD_BW_WR_MAX_ONLY = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MAX];
    HDD_LAT_RD_AVG = SSD_Stats.statistics[RD][LATENCY][AVG];
    HDD_LAT_RD_MIN = SSD_Stats.statistics[RD][LATENCY][MIN];
    HDD_LAT_RD_MAX = SSD_Stats.statistics[RD][LATENCY][MAX];
    HDD_LAT_WR_AVG = SSD_Stats.statistics[WR][LATENCY][AVG];
    HDD_LAT_WR_MIN = SSD_Stats.statistics[WR][LATENCY][MIN];
    HDD_LAT_WR_MAX = SSD_Stats.statistics[WR][LATENCY][MAX];
    HDD_IO_RD_AVG = SSD_Stats.statistics[RD][IOPS][AVG];
    HDD_IO_RD_MIN = SSD_Stats.statistics[RD][IOPS][MIN];
    HDD_IO_RD_MAX = SSD_Stats.statistics[RD][IOPS][MAX];
    HDD_IO_RD_AVG_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][AVG];
    HDD_IO_RD_MIN_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][MIN];
    HDD_IO_RD_MAX_WIDLE = SSD_Stats.statistics[RD][IOPS_WIDLE][MAX];
    HDD_IO_RD_AVG_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][AVG];
    HDD_IO_RD_MIN_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][MIN];
    HDD_IO_RD_MAX_ONLY = SSD_Stats.statistics[RD][IOPS_OPER][MAX];
    HDD_IO_WR_AVG = SSD_Stats.statistics[WR][IOPS][AVG];
    HDD_IO_WR_MIN = SSD_Stats.statistics[WR][IOPS][MIN];
    HDD_IO_WR_MAX = SSD_Stats.statistics[WR][IOPS][MAX];
    HDD_IO_WR_AVG_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][AVG];
    HDD_IO_WR_MIN_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][MIN];
    HDD_IO_WR_MAX_WIDLE = SSD_Stats.statistics[WR][IOPS_WIDLE][MAX];
    HDD_IO_WR_AVG_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][AVG];
    HDD_IO_WR_MIN_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][MIN];
    HDD_IO_WR_MAX_ONLY = SSD_Stats.statistics[WR][IOPS_OPER][MAX];
    HDD_DEVICE_IDLE = SSD_Stats.statistics[TOT][DEVICE_IDLE][TOT];
    HDD_DEVICE_BUSY = SSD_Stats.statistics[TOT][DEVICE_BUSY][TOT];

}


void
VirtIOBlock::updatePALStats()
{
    PAL_CAP_RD_AVG   = SSD_Stats.statistics[RD][CAPACITY][AVG];
    PAL_CAP_RD_MIN   = SSD_Stats.statistics[RD][CAPACITY][MIN];
    PAL_CAP_RD_MAX   = SSD_Stats.statistics[RD][CAPACITY][MAX];
    PAL_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    PAL_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    PAL_BW_RD_AVG    = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    PAL_BW_RD_MIN    = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    PAL_BW_RD_MAX    = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    PAL_BW_RD_AVG_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][AVG];
    PAL_BW_RD_MIN_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MIN];
    PAL_BW_RD_MAX_WIDLE    = SSD_Stats.statistics[RD][BANDWIDTH_WIDLE][MAX];
    PAL_BW_RD_AVG_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][AVG];
    PAL_BW_RD_MIN_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MIN];
    PAL_BW_RD_MAX_ONLY    = SSD_Stats.statistics[RD][BANDWIDTH_OPER][MAX];
    PAL_IO_RD_AVG    = SSD_Stats.statistics[RD][IOPS][AVG];
    PAL_IO_RD_MIN    = SSD_Stats.statistics[RD][IOPS][MIN];
    PAL_IO_RD_MAX    = SSD_Stats.statistics[RD][IOPS][MAX];
    PAL_IO_RD_AVG_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][AVG];
    PAL_IO_RD_MIN_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][MIN];
    PAL_IO_RD_MAX_WIDLE    = SSD_Stats.statistics[RD][IOPS_WIDLE][MAX];
    PAL_IO_RD_AVG_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][AVG];
    PAL_IO_RD_MIN_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][MIN];
    PAL_IO_RD_MAX_ONLY    = SSD_Stats.statistics[RD][IOPS_OPER][MAX];
    PAL_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG]/1000/1000;
    PAL_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN]/1000/1000;
    PAL_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX]/1000/1000;

    PAL_CAP_WR_AVG   = SSD_Stats.statistics[WR][CAPACITY][AVG];
    PAL_CAP_WR_MIN   = SSD_Stats.statistics[WR][CAPACITY][MIN];
    PAL_CAP_WR_MAX   = SSD_Stats.statistics[WR][CAPACITY][MAX];
    PAL_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    PAL_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    PAL_BW_WR_AVG    = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    PAL_BW_WR_MIN    = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    PAL_BW_WR_MAX    = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    PAL_BW_WR_AVG_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][AVG];
    PAL_BW_WR_MIN_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MIN];
    PAL_BW_WR_MAX_WIDLE    = SSD_Stats.statistics[WR][BANDWIDTH_WIDLE][MAX];
    PAL_BW_WR_AVG_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][AVG];
    PAL_BW_WR_MIN_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MIN];
    PAL_BW_WR_MAX_ONLY    = SSD_Stats.statistics[WR][BANDWIDTH_OPER][MAX];
    PAL_IO_WR_AVG    = SSD_Stats.statistics[WR][IOPS][AVG];
    PAL_IO_WR_MIN    = SSD_Stats.statistics[WR][IOPS][MIN];
    PAL_IO_WR_MAX    = SSD_Stats.statistics[WR][IOPS][MAX];
    PAL_IO_WR_AVG_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][AVG];
    PAL_IO_WR_MIN_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][MIN];
    PAL_IO_WR_MAX_WIDLE    = SSD_Stats.statistics[WR][IOPS_WIDLE][MAX];
    PAL_IO_WR_AVG_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][AVG];
    PAL_IO_WR_MIN_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][MIN];
    PAL_IO_WR_MAX_ONLY    = SSD_Stats.statistics[WR][IOPS_OPER][MAX];
    PAL_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG]/1000/1000;
    PAL_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN]/1000/1000;
    PAL_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX]/1000/1000;

    PAL_CAP_ER_AVG   = SSD_Stats.statistics[ER][CAPACITY][AVG];
    PAL_CAP_ER_MIN   = SSD_Stats.statistics[ER][CAPACITY][MIN];
    PAL_CAP_ER_MAX   = SSD_Stats.statistics[ER][CAPACITY][MAX];
    PAL_CAP_ER_TOT   = SSD_Stats.statistics[ER][CAPACITY][TOT];
    PAL_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    PAL_BW_ER_AVG    = SSD_Stats.statistics[ER][BANDWIDTH][AVG];
    PAL_BW_ER_MIN    = SSD_Stats.statistics[ER][BANDWIDTH][MIN];
    PAL_BW_ER_MAX    = SSD_Stats.statistics[ER][BANDWIDTH][MAX];
    PAL_BW_ER_AVG_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][AVG];
    PAL_BW_ER_MIN_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][MIN];
    PAL_BW_ER_MAX_WIDLE    = SSD_Stats.statistics[ER][BANDWIDTH_WIDLE][MAX];
    PAL_BW_ER_AVG_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][AVG];
    PAL_BW_ER_MIN_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][MIN];
    PAL_BW_ER_MAX_ONLY    = SSD_Stats.statistics[ER][BANDWIDTH_OPER][MAX];
    PAL_IO_ER_AVG    = SSD_Stats.statistics[ER][IOPS][AVG];
    PAL_IO_ER_MIN    = SSD_Stats.statistics[ER][IOPS][MIN];
    PAL_IO_ER_MAX    = SSD_Stats.statistics[ER][IOPS][MAX];
    PAL_IO_ER_AVG_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][AVG];
    PAL_IO_ER_MIN_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][MIN];
    PAL_IO_ER_MAX_WIDLE    = SSD_Stats.statistics[ER][IOPS_WIDLE][MAX];
    PAL_IO_ER_AVG_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][AVG];
    PAL_IO_ER_MIN_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][MIN];
    PAL_IO_ER_MAX_ONLY    = SSD_Stats.statistics[ER][IOPS_OPER][MAX];
    PAL_LAT_ER_AVG    = SSD_Stats.statistics[ER][LATENCY][AVG]/1000/1000;
    PAL_LAT_ER_MIN    = SSD_Stats.statistics[ER][LATENCY][MIN]/1000/1000;
    PAL_LAT_ER_MAX    = SSD_Stats.statistics[ER][LATENCY][MAX]/1000/1000;

    PAL_CAP_TOT_AVG   = SSD_Stats.statistics[TOT][CAPACITY][AVG];
    PAL_CAP_TOT_MIN   = SSD_Stats.statistics[TOT][CAPACITY][MIN];
    PAL_CAP_TOT_MAX   = SSD_Stats.statistics[TOT][CAPACITY][MAX];
    PAL_CAP_TOT_TOT   = SSD_Stats.statistics[TOT][CAPACITY][TOT];
    PAL_CAP_TOT_COUNT = SSD_Stats.statistics[TOT][CAPACITY][COUNT];
    PAL_BW_TOT_AVG    = SSD_Stats.statistics[TOT][BANDWIDTH][AVG];
    PAL_BW_TOT_MIN    = SSD_Stats.statistics[TOT][BANDWIDTH][MIN];
    PAL_BW_TOT_MAX    = SSD_Stats.statistics[TOT][BANDWIDTH][MAX];
    PAL_BW_TOT_AVG_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][AVG];
    PAL_BW_TOT_MIN_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MIN];
    PAL_BW_TOT_MAX_WIDLE    = SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MAX];
    PAL_IO_TOT_AVG    = SSD_Stats.statistics[TOT][IOPS][AVG];
    PAL_IO_TOT_MIN    = SSD_Stats.statistics[TOT][IOPS][MIN];
    PAL_IO_TOT_MAX    = SSD_Stats.statistics[TOT][IOPS][MAX];
    PAL_IO_TOT_AVG_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][AVG];
    PAL_IO_TOT_MIN_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][MIN];
    PAL_IO_TOT_MAX_WIDLE    = SSD_Stats.statistics[TOT][IOPS_WIDLE][MAX];
    PAL_LAT_TOT_AVG    = SSD_Stats.statistics[TOT][LATENCY][AVG]/1000/1000;
    PAL_LAT_TOT_MIN    = SSD_Stats.statistics[TOT][LATENCY][MIN]/1000/1000;
    PAL_LAT_TOT_MAX    = SSD_Stats.statistics[TOT][LATENCY][MAX]/1000/1000;

    PAL_DEVICE_IDLE = SSD_Stats.statistics[TOT][DEVICE_IDLE][TOT];
    PAL_DEVICE_BUSY = SSD_Stats.statistics[TOT][DEVICE_BUSY][TOT];
}


void
VirtIOBlock::updateFTLStats()
{
    FTL_HOST_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    FTL_HOST_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    FTL_HOST_BW_RD_AVG    = SSD_Stats.statistics[RD][BANDWIDTH][AVG];
    FTL_HOST_BW_RD_MIN    = SSD_Stats.statistics[RD][BANDWIDTH][MIN];
    FTL_HOST_BW_RD_MAX    = SSD_Stats.statistics[RD][BANDWIDTH][MAX];
    FTL_HOST_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG];
    FTL_HOST_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN];
    FTL_HOST_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX];
    FTL_HOST_IO_RD_AVG    = SSD_Stats.statistics[RD][IOPS][AVG];
    FTL_HOST_IO_RD_MIN    = SSD_Stats.statistics[RD][IOPS][MIN];
    FTL_HOST_IO_RD_MAX    = SSD_Stats.statistics[RD][IOPS][MAX];
    FTL_HOST_SIZE_RD_AVG    = SSD_Stats.statistics[RD][SIZE][AVG];
    FTL_HOST_SIZE_RD_MIN    = SSD_Stats.statistics[RD][SIZE][MIN];
    FTL_HOST_SIZE_RD_MAX    = SSD_Stats.statistics[RD][SIZE][MAX];

    FTL_HOST_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    FTL_HOST_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    FTL_HOST_BW_WR_AVG    = SSD_Stats.statistics[WR][BANDWIDTH][AVG];
    FTL_HOST_BW_WR_MIN    = SSD_Stats.statistics[WR][BANDWIDTH][MIN];
    FTL_HOST_BW_WR_MAX    = SSD_Stats.statistics[WR][BANDWIDTH][MAX];
    FTL_HOST_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG];
    FTL_HOST_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN];
    FTL_HOST_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX];
    FTL_HOST_IO_WR_AVG    = SSD_Stats.statistics[WR][IOPS][AVG];
    FTL_HOST_IO_WR_MIN    = SSD_Stats.statistics[WR][IOPS][MIN];
    FTL_HOST_IO_WR_MAX    = SSD_Stats.statistics[WR][IOPS][MAX];
    FTL_HOST_SIZE_WR_AVG    = SSD_Stats.statistics[WR][SIZE][AVG];
    FTL_HOST_SIZE_WR_MIN    = SSD_Stats.statistics[WR][SIZE][MIN];
    FTL_HOST_SIZE_WR_MAX    = SSD_Stats.statistics[WR][SIZE][MAX];

    FTL_HOST_BW_TOT_AVG_WIDLE =
      SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][AVG];
    FTL_HOST_BW_TOT_MIN_WIDLE =
      SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MIN];
    FTL_HOST_BW_TOT_MAX_WIDLE =
      SSD_Stats.statistics[TOT][BANDWIDTH_WIDLE][MAX];
    FTL_HOST_BW_TOT_AVG	= SSD_Stats.statistics[TOT][BANDWIDTH][AVG];
    FTL_HOST_BW_TOT_MIN	= SSD_Stats.statistics[TOT][BANDWIDTH][MIN];
    FTL_HOST_BW_TOT_MAX	= SSD_Stats.statistics[TOT][BANDWIDTH][MAX];

    FTL_HOST_IO_TOT_AVG_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][AVG];
    FTL_HOST_IO_TOT_MIN_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][MIN];
    FTL_HOST_IO_TOT_MAX_WIDLE 	= SSD_Stats.statistics[TOT][IOPS_WIDLE][MAX];
    FTL_HOST_IO_TOT_AVG		 	= SSD_Stats.statistics[TOT][IOPS][AVG];
    FTL_HOST_IO_TOT_MIN		 	= SSD_Stats.statistics[TOT][IOPS][MIN];
    FTL_HOST_IO_TOT_MAX		 	= SSD_Stats.statistics[TOT][IOPS][MAX];

}

void
VirtIOBlock::updateFTL_PAL_Stats()
{
    FTL_PAL_CAP_RD_TOT   = SSD_Stats.statistics[RD][CAPACITY][TOT];
    FTL_PAL_CAP_RD_COUNT = SSD_Stats.statistics[RD][CAPACITY][COUNT];
    FTL_PAL_CAP_WR_TOT   = SSD_Stats.statistics[WR][CAPACITY][TOT];
    FTL_PAL_CAP_WR_COUNT = SSD_Stats.statistics[WR][CAPACITY][COUNT];
    FTL_PAL_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    FTL_PAL_LAT_RD_AVG    = SSD_Stats.statistics[RD][LATENCY][AVG];
    FTL_PAL_LAT_RD_MIN    = SSD_Stats.statistics[RD][LATENCY][MIN];
    FTL_PAL_LAT_RD_MAX    = SSD_Stats.statistics[RD][LATENCY][MAX];
    FTL_PAL_LAT_WR_AVG    = SSD_Stats.statistics[WR][LATENCY][AVG];
    FTL_PAL_LAT_WR_MIN    = SSD_Stats.statistics[WR][LATENCY][MIN];
    FTL_PAL_LAT_WR_MAX    = SSD_Stats.statistics[WR][LATENCY][MAX];
    FTL_PAL_LAT_ER_AVG    = SSD_Stats.statistics[ER][LATENCY][AVG];
    FTL_PAL_LAT_ER_MIN    = SSD_Stats.statistics[ER][LATENCY][MIN];
    FTL_PAL_LAT_ER_MAX    = SSD_Stats.statistics[ER][LATENCY][MAX];
}

void
VirtIOBlock::updateFTL_MAP_Stats()
{
    FTL_MAP_CAP_GC_COUNT = SSD_Stats.statistics[GC][CAPACITY][COUNT];
    FTL_MAP_CAP_ER_COUNT = SSD_Stats.statistics[ER][CAPACITY][COUNT];
    FTL_MAP_LAT_GC_AVG    = SSD_Stats.statistics[GC][LATENCY][AVG];
    FTL_MAP_LAT_GC_MIN    = SSD_Stats.statistics[GC][LATENCY][MIN];
    FTL_MAP_LAT_GC_MAX    = SSD_Stats.statistics[GC][LATENCY][MAX];
}


void
VirtIOBlock::schedPrint()
{
  hil->printStats();
  schedule(schPrintEvent, curTick() + 100000000000);
}



VirtIOBlock::~VirtIOBlock()
{
}

void
VirtIOBlock::readConfig(PacketPtr pkt, Addr cfgOffset)
{
    Config cfg_out;
    cfg_out.capacity = htov_legacy(config.capacity);

    readConfigBlob(pkt, cfgOffset, (uint8_t *)&cfg_out);
}

VirtIOBlock::Status
VirtIOBlock::read(const BlkRequest &req, VirtDescriptor *desc_chain,
                  size_t off_data, size_t size)
{
    uint8_t data[size];
    uint64_t sector(req.sector);
    int sec_count = 0;

    DPRINTF(VIOBlock, "Read request starting @ sector %i (size: %i)\n",
            sector, size);

    if (size % SectorSize != 0)
        panic("Unexpected request/sector size relationship\n");

    for (Addr offset = 0; offset < size; offset += SectorSize) {
        if (image.read(data + offset, sector) != SectorSize) {
            warn("Failed to read sector %i\n", sector);
            return S_IOERR;
        }
        ++sector;
        ++sec_count;
    }

    hil->SSDoperation(req.sector, sec_count, curTick(), true);

    std::map<Addr, Tick>::iterator iDelay = hil->delayMap.find(req.sector);
    if (iDelay == hil->delayMap.end()){
      printf("the address is %lu \n", req.sector);
      panic("Error: Flag Delay Fault\n");
    }

    ssd_latency = iDelay->second;
    iDelay->second = 0;
    statsUpdate();
    //ssd_latency = 1000000;

    desc_chain->chainWrite(off_data, data, size);

    return S_OK;
}

VirtIOBlock::Status
VirtIOBlock::write(const BlkRequest &req, VirtDescriptor *desc_chain,
                  size_t off_data, size_t size)
{
    uint8_t data[size];
    uint64_t sector(req.sector);
    int sec_count = 0;

    DPRINTF(VIOBlock, "Write request starting @ sector %i (size: %i)\n",
            sector, size);

    if (size % SectorSize != 0)
        panic("Unexpected request/sector size relationship\n");


    desc_chain->chainRead(off_data, data, size);

    for (Addr offset = 0; offset < size; offset += SectorSize) {
        if (image.write(data + offset, sector) != SectorSize) {
            warn("Failed to write sector %i\n", sector);
            return S_IOERR;
        }
        ++sector;
        ++sec_count;
    }


    hil->SSDoperation(req.sector, sec_count, curTick(), false);

    std::map<Addr, Tick>::iterator iDelay = hil->delayMap.find(req.sector);
    if (iDelay == hil->delayMap.end()){
      printf("the address is %lu \n", req.sector);
      panic("Error: Flag Delay Fault\n");
    }

    ssd_latency = iDelay->second;
    iDelay->second = 0;
    statsUpdate();
    //ssd_latency = 1000000;

    return S_OK;

}

void
VirtIOBlock::RequestQueue::onNotifyDescriptor(VirtDescriptor *desc)
{
    DPRINTF(VIOBlock, "Got input data descriptor (len: %i)\n",
            desc->size());
    /*
     * Read the request structure and do endian conversion if
     * necessary.
     */
    BlkRequest req;
    desc->chainRead(0, (uint8_t *)&req, sizeof(req));
    req.type = htov_legacy(req.type);
    req.sector = htov_legacy(req.sector);

    Status status;
    const size_t data_size(desc->chainSize()
                           - sizeof(BlkRequest) - sizeof(Status));

    switch (req.type) {
      case T_IN:
        status = parent.read(req, desc, sizeof(BlkRequest), data_size);
        break;

      case T_OUT:
        status = parent.write(req, desc, sizeof(BlkRequest), data_size);
        break;

      case T_FLUSH:
        status = S_OK;
        break;

      default:
        warn("Unsupported IO request: %i\n", req.type);
        status = S_UNSUPP;
        break;
    }

    desc->chainWrite(sizeof(BlkRequest) + data_size,
                     &status, sizeof(status));
    parent.process_desc(desc);
    /*
    Tick desc_tick = curTick() + ssd_latency;
    desc_map.insert(std::pair<Tick, VirtDescriptor *>(desc_tick, desc));
    */
    // Tell the guest that we are done with this descriptor.
    //produceDescriptor(desc, sizeof(BlkRequest) + data_size + sizeof(Status));
    //parent.kick();
}

VirtIOBlock *
VirtIOBlockParams::create()
{
    return new VirtIOBlock(this);
}


void
VirtIOBlock::process_desc(VirtDescriptor * insert_desc)
{
    Tick desc_tick = curTick() + ssd_latency;
    std::map <Tick, VirtDescriptor *>::iterator it = desc_map.find(desc_tick);
    while (it != desc_map.end()) {
      desc_tick = desc_tick + 1000;
      it = desc_map.find(desc_tick);
    }
    std::pair<Tick, VirtDescriptor *> desc_pair;
    desc_pair = std::make_pair(desc_tick, insert_desc);
    if (desc_map.empty()) {
      schedule(vioResponseEvent, desc_tick);
    } else {
      it = desc_map.begin();
      if (it->first > desc_tick)
        reschedule(vioResponseEvent, desc_tick);
    }
    desc_map.insert(desc_pair);
}

void
VirtIOBlock::vioResponse()
{
    Tick cur_tick = curTick();
    std::map <Tick, VirtDescriptor *>::iterator it = desc_map.find(cur_tick);
    if (it == desc_map.end())
      panic("Missing descriptor at %llu\n", cur_tick);
    VirtDescriptor * cur_desc = it->second;
    const size_t data_size(cur_desc->chainSize()
                           - sizeof(BlkRequest) - sizeof(Status));
    qRequests.produceDescriptor(cur_desc, sizeof(BlkRequest)
                                + data_size + sizeof(Status));
    kick();
    desc_map.erase(it);
    if (!desc_map.empty()) {
      it = desc_map.begin();
      schedule(vioResponseEvent, it->first);
    }
}
