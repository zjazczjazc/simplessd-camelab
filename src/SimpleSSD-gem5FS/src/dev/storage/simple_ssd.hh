#ifndef __ssdsim_h__
#define __ssdsim_h__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

#include "dev/storage/simple_ssd_types.hh"

using namespace std;

#include "dev/storage/config_reader.hh"
#include "dev/storage/ftl.hh"
#include "dev/storage/global_config.hh"
#include "dev/storage/latency.hh"
#include "dev/storage/latency_MLC.hh"
#include "dev/storage/latency_SLC.hh"
#include "dev/storage/latency_TLC.hh"
#include "dev/storage/pal2.hh"
#include "dev/storage/pal_statistics.hh"
#include "dev/storage/simulator.hh"

#endif //__ssdsims_h__
