//
//  Block.h
//  FTLSim_functional
//
//  Created by Narges on 6/24/15.
//  Copyright (c) 2015 narges shahidi. All rights reserved.
//

#ifndef FTLSim_functional_Block_h
#define FTLSim_functional_Block_h

#include <iostream>

#include "dev/storage/ftl_defs.hh"

#define INT_SIZE 32



class Block{
private:

public:
  // since page_bit_map is assigning one bit for each
  // page, state should be only zero or one
    enum            PAGE_STATE {PAGE_VALID = 0, PAGE_INVALID, PAGE_FREE};
  // total number of page per block
    int             page_per_block;
  // physical block number
    Addr             block_number;
  // number of erase count on the block
    int             erase_count;
   // each bit shows the state of a page
  // value for each bit: (PAGE_VALID vs. PAGE_INVALID)
  // since each block has more than 32 (int size) bit,
  // an array of integer is used
    unsigned int*   page_bit_map;
  // page within the block have to be written in order
    Addr             page_sequence_number;
    bool bad_block; // true: bad_block false:good_block


    Block 		 (){}
  // initialize page_bit_map, etc.
    void initialize		 (int page_per_block, Addr bn);
  // add erase count, reset other variables to initial state
    void erase_block     ();
  // change page state: PAGE_VALID or PAGE_INVALID
    void set_page_state  (int page_offset, PAGE_STATE state);
  // get the page state, return PAGE_VALID or
  // PAGE_INVALID or PAGE_FREE
    int get_page_state   (int page_offset);
  // return true if block is empty, otherwise, false.
    bool is_empty        ();
  // return true if no free page remained in the block
    bool is_full         ();
  // write a new page into the block.
  // if page_offset == -1: write using page_sequence_number
  // otherwise, invalid all free pages until reach to
  // this page
    STATE write_page     (Addr logical_page, int &page_offset);
  // return the number of valid pages within the block
    int valid_page_count ();
  // return number of free pages inside the block
    int free_page_count  ();
  // some print form of the block (use for debug)
    void to_string       ();

};


#endif
