#include "dev/storage/pal2_timeslot.hh"

TimeSlot::TimeSlot(uint64 startTick, uint64 duration)
{
    StartTick = startTick;
    EndTick = startTick + duration - 1;
    Next = NULL;
}
