#ifndef __PAL2_h__
#define __PAL2_h__

#include "dev/storage/global_config.hh"
#include "dev/storage/latency.hh"
#include "dev/storage/pal_statistics.hh"
#include "dev/storage/simple_ssd_types.hh"

//#include "RequestQueue2.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

#include "dev/storage/ftl.hh" //for using Command Queue in FTL
#include "dev/storage/pal2_timeslot.hh"
#include "dev/storage/simulator.hh"

using namespace std;

class PALStatistics;
//Globals
extern GlobalConfig* gconf;
extern Latency* lat;
extern Simulator* sim;

class PAL2 //let's not inherit PAL1
{
    public:
        PAL2(FTL* ftl, PALStatistics* statistics);
        ~PAL2();

        TimeSlot** ChTimeSlots;
        TimeSlot** DieTimeSlots;
        TimeSlot** MergedTimeSlots; //for gathering busy time

        std::map<uint64, uint64> OpTimeStamp[3];

        std::map<uint64, std::map<uint64, uint64>* > * ChFreeSlots;
        uint64* ChStartPoint; //record the start point of rightmost free slot
        std::map<uint64, std::map<uint64, uint64>* > * DieFreeSlots;
        uint64* DieStartPoint;

        FTL* ftl_core;
        void FetchQueue();
        void TimelineScheduling(Command* req);
        PALStatistics* stats; //statistics of PAL2, not created by itself
        void InquireBusyTime(uint64 currentTick);
        void FlushTimeSlots(uint64 currentTick);
        void FlushOpTimeStamp();
        TimeSlot* FlushATimeSlot(TimeSlot* tgtTimeSlot, uint64 currentTick);
        TimeSlot* FlushATimeSlotBusyTime(TimeSlot* tgtTimeSlot,
                                         uint64 currentTick, uint64* TimeSum);
        //Jie: merge time slots
        void MergeATimeSlot(TimeSlot* tgtTimeSlot);
        void MergeATimeSlot(TimeSlot* startTimeSlot, TimeSlot* endTimeSlot);
        void MergeATimeSlotCH(TimeSlot* tgtTimeSlot);
        void MergeATimeSlotDIE(TimeSlot* tgtTimeSlot);
        TimeSlot* InsertAfter(TimeSlot* tgtTimeSlot, uint64 tickLen,
                              uint64 tickFrom);
  // you can insert a tickLen TimeSlot after Returned TimeSlot.
        TimeSlot* FindFreeTime(TimeSlot* tgtTimeSlot, uint64 tickLen,
                               uint64 tickFrom);

        //Jie: return: FreeSlot is found?
        bool FindFreeTime(std::map<uint64,
                          std::map<uint64, uint64>* >& tgtFreeSlot,
                          uint64 tickLen, uint64 & tickFrom,
                          uint64 & startTick, bool & conflicts);
        void InsertFreeSlot(std::map<uint64,
                            std::map<uint64, uint64>* >& tgtFreeSlot,
                            uint64 tickLen, uint64 tickFrom, uint64 startTick,
                            uint64 & startPoint, bool split);
        void AddFreeSlot(std::map<uint64,
                         std::map<uint64, uint64>* >& tgtFreeSlot,
                         uint64 tickLen, uint64 tickFrom);
        void FlushFreeSlots(uint64 currentTick);
        void FlushAFreeSlot(std::map<uint64,
                            std::map<uint64, uint64>* >& tgtFreeSlot,
                            uint64 currentTick);
        uint8 VerifyTimeLines(uint8 print_on);


        //PPN Conversion related //ToDo: Shifted-Mode is
        //also required for better performance.
        uint32 RearrangedSizes[7];
        uint32 CPDPBPtoDieIdx(CPDPBP* pCPDPBP);
        void printCPDPBP(CPDPBP* pCPDPBP);
        void PPNdisassemble(uint64* pPPN, CPDPBP* pCPDPBP);
        void AssemblePPN(CPDPBP* pCPDPBP, uint64* pPPN);

};

#endif
