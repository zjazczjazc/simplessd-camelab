#ifndef __PAL2_TimeSlot_h__
#define __PAL2_TimeSlot_h__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

#include "dev/storage/simple_ssd_types.hh"

using namespace std;

class TimeSlot
{
    public:
        TimeSlot(uint64 startTick, uint64 duration);
        uint64 StartTick;
        uint64 EndTick;
        TimeSlot* Next;
};

#endif
