#ifndef __GlobalConfig_h__
#define __GlobalConfig_h__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>

#include "arch/isa_traits.hh"
#include "base/types.hh"
#include "dev/storage/config_reader.hh"
#include "dev/storage/simple_ssd_types.hh"

using namespace std;

class ConfigReader;
/*==============================
    GlobalConfig
==============================*/
class GlobalConfig
{
    private:

    public:
        uint8  NANDType;
        uint32 NumChannel;  //in a ssd
        uint32 NumPackage;  //in a channel
        uint32 NumDie;      //in a package
        uint32 NumPlane;    //in a die
        uint32 NumBlock;    //in a plane
        uint32 NumPage;     //in a block
        uint32 SizePage;    //in a page
        uint8  AddrSeq[7];
        uint8  AddrRemap[6];  //PAL: address remapping information
        uint32 DMAMHz;      //DMA_MHz
        long double FTLOP;
        long double FTLGCThreshold;
        uint32 FTLMapN;
        uint32 FTLMapK;
        uint32 FTLEraseCycle;
        int SuperblockDegree;
        double Warmup;

        uint8  EnableDMAPreemption; //DMA Preemption Option

        uint32 OriginalSizes[7];


        GlobalConfig(ConfigReader*  cr );

        void PrintInfo();
        uint64 GetTotalSizeSSD();
        uint64 GetTotalNumPage();
        uint64 GetTotalNumBlock();
        uint64 GetTotalNumDie();
        uint64 GetTotalNumPlane();
};

#endif //__GlobalConfig_h__
