#!/bin/bash

# Get command line input. We will need to check these.
BENCHMARK=$1                    # Benchmark name, e.g. bzip2

cd /Path/to/simplessd/src/gem5SE

pwd

bash ./SPEC06/$BENCHMARK/run_gem5_ARM_spec06.sh $BENCHMARK
