import m5
from m5.objects import *

# These three directory paths are not currently used.
#gem5_dir = '<FULL_PATH_TO_YOUR_GEM5_INSTALL>'
#spec_dir = '<FULL_PATH_TO_YOUR_SPEC_CPU2006_INSTALL>'
#out_dir = '<FULL_PATH_TO_DESIRED_OUTPUT_DIRECTORY>'
spec_dir = '/home/jie/workspace/simpleSSD/workloads/SPEC06/benchspec/CPU2006/'
rest_dir = '/run/build_base_amd64-m64-gcc42-nn.0000/'
#alpha_suffix = '_base.my-alpha'
alpha_suffix = '_base.amd64-m64-gcc42-nn'
#alpha_suffix = ''
#temp
#binary_dir = spec_dir
data_dir = spec_dir
#alpha_suffix = ''
#400.perlbench
perlbench = LiveProcess()
perlbench.executable = spec_dir + '400.perlbench' + '/exe/' + 'perlbench' + alpha_suffix
# TEST CMDS
#perlbench.cmd = [perlbench.executable] + ['-I.', '-I./lib', 'attrs.pl']
# REF CMDS
#perlbench.cmd = [perlbench.executable] + ['-I./lib', 'checkspam.pl', '2500', '5', '25', '11', '150', '1', '1', '1', '1']
perlbench.cmd = [perlbench.executable] + ['-I./lib', 'diffmail.pl', '4', '800', '10', '17', '19', '300']
#perlbench.cmd = [perlbench.executable] + ['-I./lib', 'splitmail.pl', '1600', '12', '26', '16', '4500']
#perlbench.output = out_dir+'perlbench.out'

#401.bzip2
bzip2 = LiveProcess()
#bzip2.executable =  'bzip2' + alpha_suffix
bzip2.executable =  spec_dir + '401.bzip2' + '/exe/' + 'bzip2' + alpha_suffix
#bzip2.executable =  spec_dir + '401.bzip2' + rest_dir + 'bzip2' #+ alpha_suffix
# TEST CMDS
#bzip2.cmd = [bzip2.executable] + ['input.program', '5']
# REF CMDS
bzip2.cmd = [bzip2.executable] + ['input.source', '280']
#bzip2.cmd = [bzip2.executable] + ['chicken.jpg', '30']
#bzip2.cmd = [bzip2.executable] + ['liberty.jpg', '30']
#bzip2.cmd = [bzip2.executable] + ['input.program', '280']
#bzip2.cmd = [bzip2.executable] + ['text.html', '280']
#bzip2.cmd = [bzip2.executable] + ['input.combined', '200']
#bzip2.output = out_dir + 'bzip2.out'

#403.gcc
gcc = LiveProcess()
gcc.executable = spec_dir +'403.gcc' + '/exe/' + 'gcc' + alpha_suffix
# TEST CMDS
#gcc.cmd = [gcc.executable] + ['cccp.i', '-o', 'cccp.s']
# REF CMDS
gcc.cmd = [gcc.executable] + ['166.i', '-o', '166.s']
#gcc.cmd = [gcc.executable] + ['200.i', '-o', '200.s']
#gcc.cmd = [gcc.executable] + ['c-typeck.i', '-o', 'c-typeck.s']
#gcc.cmd = [gcc.executable] + ['cp-decl.i', '-o', 'cp-decl.s']
#gcc.cmd = [gcc.executable] + ['expr.i', '-o', 'expr.s']
#gcc.cmd = [gcc.executable] + ['expr2.i', '-o', 'expr2.s']
#gcc.cmd = [gcc.executable] + ['g23.i', '-o', 'g23.s']
#gcc.cmd = [gcc.executable] + ['s04.i', '-o', 's04.s']
#gcc.cmd = [gcc.executable] + ['scilab.i', '-o', 'scilab.s']
#gcc.output = out_dir + 'gcc.out'

#410.bwaves
bwaves = LiveProcess()
bwaves.executable = spec_dir + '410.bwaves' + '/exe/' + 'bwaves' + alpha_suffix
#bwaves.executable = spec_dir + '410.bwaves' + rest_dir + 'bwaves' #+ alpha_suffix
#bwaves.input = 'bwaves.in'
#bwaves.executable = 'bwaves' + alpha_suffix
# TEST CMDS
#bwaves.cmd = [bwaves.executable]
# REF CMDS
bwaves.cmd = [bwaves.executable]
#bwaves.input = 'bwaves.in'
#bwaves.output = out_dir + 'bwaves.out'

#416.gamess
gamess=LiveProcess()
gamess.executable = spec_dir +'416.gamess' + '/exe/' + 'gamess' + alpha_suffix
# TEST CMDS
#gamess.cmd = [gamess.executable]
#gamess.input = 'exam29.config'
# REF CMDS
gamess.cmd = [gamess.executable]
gamess.input = 'cytosine.2.config'
#gamess.cmd = [gamess.executable]
#gamess.input = 'h2ocu2+.gradient.config'
#gamess.cmd = [gamess.executable]
#gamess.input = 'triazolium.config'
#gamess.output = out_dir + 'gamess.out'

#429.mcf
mcf = LiveProcess()
mcf.executable = spec_dir +'429.mcf' + '/exe/' +  'mcf' + alpha_suffix
# TEST CMDS
#mcf.cmd = [mcf.executable] + ['inp.in']
# REF CMDS
mcf.cmd = [mcf.executable] + ['inp.in']
#mcf.output = out_dir + 'mcf.out'

#433.milc
milc=LiveProcess()
milc.executable = spec_dir + '433.milc' + '/exe/' + 'milc' + alpha_suffix
# TEST CMDS
#milc.cmd = [milc.executable]
#milc.input = 'su3imp.in'
# REF CMDS
milc.cmd = [milc.executable]
milc.input = 'su3imp.in'
#milc.output = out_dir + 'milc.out'

#434.zeusmp
zeusmp=LiveProcess()
zeusmp.executable = spec_dir +'434.zeusmp' + '/exe/' +  'zeusmp' + alpha_suffix
# TEST CMDS
#zeusmp.cmd = [zeusmp.executable]
# REF CMDS
zeusmp.cmd = [zeusmp.executable]
#zeusmp.output = out_dir + 'zeusmp.out'

#435.gromacs
gromacs = LiveProcess()
gromacs.executable = spec_dir +'435.gromacs' + '/exe/' + 'gromacs' + alpha_suffix
# TEST CMDS
#gromacs.cmd = [gromacs.executable] + ['-silent','-deffnm', 'gromacs', '-nice','0']
# REF CMDS
gromacs.cmd = [gromacs.executable] + ['-silent','-deffnm', 'gromacs', '-nice','0']
#gromacs.output = out_dir + 'gromacs.out'

#436.cactusADM
cactusADM = LiveProcess()
cactusADM.executable = spec_dir +'436.cactusADM'  + '/exe/' + 'cactusADM' + alpha_suffix
# TEST CMDS
#cactusADM.cmd = [cactusADM.executable] + ['benchADM.par']
# REF CMDS
cactusADM.cmd = [cactusADM.executable] + ['benchADM.par']
#cactusADM.output = out_dir + 'cactusADM.out'

#437.leslie3d
leslie3d=LiveProcess()
leslie3d.executable = spec_dir +'437.leslie3d' + '/exe/' + 'leslie3d' + alpha_suffix
# TEST CMDS
#leslie3d.cmd = [leslie3d.executable]
#leslie3d.input = 'leslie3d.in'
# REF CMDS
leslie3d.cmd = [leslie3d.executable]
leslie3d.input = 'leslie3d.in'
#leslie3d.output = out_dir + 'leslie3d.out'

#444.namd
namd = LiveProcess()
namd.executable = spec_dir +'444.namd' + '/exe/' + 'namd' + alpha_suffix
# TEST CMDS
#namd.cmd = [namd.executable] + ['--input', 'namd.input', '--output', 'namd.out', '--iterations', '1']
# REF CMDS
namd.cmd = [namd.executable] + ['--input', 'namd.input', '--output', 'namd.out', '--iterations', '38']
#namd.output = out_dir + 'namd.out'

#445.gobmk
gobmk=LiveProcess()
gobmk.executable = spec_dir +'445.gobmk' + '/exe/' + 'gobmk' + alpha_suffix
# TEST CMDS
#gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
#gobmk.input = 'dniwog.tst'
# REF CMDS
gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
gobmk.input = '13x13.tst'
#gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
#gobmk.input = 'nngs.tst'
#gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
#gobmk.input = 'score2.tst'
#gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
#gobmk.input = 'trevorc.tst'
#gobmk.cmd = [gobmk.executable] + ['--quiet','--mode', 'gtp']
#gobmk.input = 'trevord.tst'
#gobmk.output = out_dir + 'gobmk.out'

#447.dealII
####### NOT WORKING #########
dealII=LiveProcess()
dealII.executable = spec_dir +'447.dealII' + '/exe/' + 'dealII' + alpha_suffix
# TEST CMDS
####### NOT WORKING #########
#dealII.cmd = [gobmk.executable]+['8']
# REF CMDS
####### NOT WORKING #########
#dealII.output = out_dir + 'dealII.out'

#450.soplex
soplex=LiveProcess()
soplex.executable = spec_dir +'450.soplex' + '/exe/' + 'soplex' + alpha_suffix
# TEST CMDS
#soplex.cmd = [soplex.executable] + ['-m10000', 'test.mps']
# REF CMDS
soplex.cmd = [soplex.executable] + ['-m45000', 'pds-50.mps']
#soplex.cmd = [soplex.executable] + ['-m3500', 'ref.mps']
#soplex.output = out_dir + 'soplex.out'

#453.povray
povray=LiveProcess()
povray.executable = spec_dir +'453.povray' + '/exe/' + 'povray' + alpha_suffix
# TEST CMDS
#povray.cmd = [povray.executable] + ['SPEC-benchmark-test.ini']
# REF CMDS
povray.cmd = [povray.executable] + ['SPEC-benchmark-ref.ini']
#povray.output = out_dir + 'povray.out'

#454.calculix
calculix=LiveProcess()
calculix.executable = spec_dir +'454.calculix' + '/exe/' + 'calculix' + alpha_suffix
# TEST CMDS
#calculix.cmd = [calculix.executable] + ['-i', 'beampic']
# REF CMDS
calculix.cmd = [calculix.executable] + ['-i', 'hyperviscoplastic']
#calculix.output = out_dir + 'calculix.out'

#456.hmmer
hmmer=LiveProcess()
hmmer.executable = spec_dir +'456.hmmer' + '/exe/' + 'hmmer' + alpha_suffix
# TEST CMDS
#hmmer.cmd = [hmmer.executable] + ['--fixed', '0', '--mean', '325', '--num', '45000', '--sd', '200', '--seed', '0', 'bombesin.hmm']
# REF CMDS
hmmer.cmd = [hmmer.executable] + ['nph3.hmm', 'swiss41']
#hmmer.cmd = [hmmer.executable] + ['--fixed', '0', '--mean', '500', '--num', '500000', '--sd', '350', '--seed', '0', 'retro.hmm']
#hmmer.output = out_dir + 'hmmer.out'

#458.sjeng
sjeng=LiveProcess()
sjeng.executable = spec_dir +'458.sjeng' + '/exe/' + 'sjeng' + alpha_suffix
# TEST CMDS
#sjeng.cmd = [sjeng.executable] + ['test.txt']
# REF CMDS
sjeng.cmd = [sjeng.executable] + ['ref.txt']
#sjeng.output = out_dir + 'sjeng.out'

#459.GemsFDTD
GemsFDTD=LiveProcess()
GemsFDTD.executable = spec_dir +'459.GemsFDTD' + '/exe/' + 'GemsFDTD' + alpha_suffix
# TEST CMDS
#GemsFDTD.cmd = [GemsFDTD.executable]
# REF CMDS
GemsFDTD.cmd = [GemsFDTD.executable]
#GemsFDTD.output = out_dir + 'GemsFDTD.out'

#462.libquantum
libquantum=LiveProcess()
libquantum.executable = spec_dir + '462.libquantum' + '/exe/' + 'libquantum' + alpha_suffix
# TEST CMDS
#libquantum.cmd = [libquantum.executable] + ['33','5']
# REF CMDS
libquantum.cmd = [libquantum.executable] + ['1297','8']
#libquantum.output = out_dir + 'libquantum.out'

#464.h264ref
h264ref=LiveProcess()
h264ref.executable = spec_dir +'464.h264ref' + '/exe/' + 'h264ref' + alpha_suffix
# TEST CMDS
#h264ref.cmd = [h264ref.executable] + ['-d', 'foreman_test_encoder_baseline.cfg']
# REF CMDS
h264ref.cmd = [h264ref.executable] + ['-d', 'foreman_ref_encoder_baseline.cfg']
#h264ref.cmd = [h264ref.executable] + ['-d', 'foreman_ref_encoder_main.cfg']
#h264ref.cmd = [h264ref.executable] + ['-d', 'sss_encoder_main.cfg']
#h264ref.output = out_dir + 'h264ref.out'

#465.tonto
tonto=LiveProcess()
tonto.executable = spec_dir +'465.tonto' + '/exe/' + 'tonto' + alpha_suffix
# TEST CMDS
#tonto.cmd = [tonto.executable]
# REF CMDS
tonto.cmd = [tonto.executable]
#tonto.output = out_dir + 'tonto.out'

#470.lbm
lbm=LiveProcess()
lbm.executable = spec_dir +'470.lbm' + '/exe/' + 'lbm' + alpha_suffix
# TEST CMDS
#lbm.cmd = [lbm.executable] + ['20', 'reference.dat', '0', '1', '100_100_130_cf_a.of']
# REF CMDS
lbm.cmd = [lbm.executable] + ['300', 'reference.dat', '0', '0', '100_100_130_ldc.of']
#lbm.output = out_dir + 'lbm.out'

#471.omnetpp
omnetpp=LiveProcess()
omnetpp.executable = spec_dir +'471.omnetpp' + '/exe/' + 'omnetpp' + alpha_suffix
# TEST CMDS
#omnetpp.cmd = [omnetpp.executable] + ['omnetpp.ini']
# REF CMDS
omnetpp.cmd = [omnetpp.executable] + ['omnetpp.ini']
#omnetpp.output = out_dir + 'omnetpp.out'

#473.astar
astar=LiveProcess()
astar.executable = spec_dir +'473.astar' + '/exe/' + 'astar' + alpha_suffix
# TEST CMDS
#astar.cmd = [astar.executable] + ['lake.cfg']
# REF CMDS
astar.cmd = [astar.executable] + ['rivers.cfg']
#astar.output = out_dir + 'astar.out'

#481.wrf
wrf=LiveProcess()
wrf.executable = spec_dir +'481.wrf' + '/exe/' + 'wrf' + alpha_suffix
# TEST CMDS
#wrf.cmd = [wrf.executable]
# REF CMDS
wrf.cmd = [wrf.executable]
#wrf.output = out_dir + 'wrf.out'

#482.sphinx3
sphinx3=LiveProcess()
sphinx3.executable = spec_dir +'482.sphinx3' + '/exe/' + 'sphinx_livepretend' + alpha_suffix
# TEST CMDS
#sphinx3.cmd = [sphinx3.executable] + ['ctlfile', '.', 'args.an4']
# REF CMDS
sphinx3.cmd = [sphinx3.executable] + ['ctlfile', '.', 'args.an4']
#sphinx3.output = out_dir + 'sphinx3.out'

#483.xalancbmk
######## NOT WORKING ###########
xalancbmk=LiveProcess()
xalancbmk.executable = spec_dir +'483.xalancbmk' + '/exe/' + 'xalancbmk' + alpha_suffix
# TEST CMDS
######## NOT WORKING ###########
#xalancbmk.cmd = [xalancbmk.executable] + ['-v','test.xml','xalanc.xsl']
# REF CMDS
######## NOT WORKING ###########
#xalancbmk.output = out_dir + 'xalancbmk.out'

#998.specrand
specrand_i=LiveProcess()
specrand_i.executable = spec_dir +'998.specrand' + '/exe/' + 'specrand' + alpha_suffix
# TEST CMDS
#specrand_i.cmd = [specrand_i.executable] + ['324342', '24239']
# REF CMDS
specrand_i.cmd = [specrand_i.executable] + ['1255432124', '234923']
#specrand_i.output = out_dir + 'specrand_i.out'

#999.specrand
specrand_f=LiveProcess()
specrand_f.executable = spec_dir +'999.specrand' + '/exe/' + 'specrand' + alpha_suffix
# TEST CMDS
#specrand_f.cmd = [specrand_f.executable] + ['324342', '24239']
# REF CMDS
specrand_f.cmd = [specrand_f.executable] + ['1255432124', '234923']
#specrand_f.output = out_dir + 'specrand_f.out'

#Breadth-First Search
BFS=LiveProcess()
BFS.executable = spec_dir + 'BFS' + '/exe/' + 'bfs'
BFS.cmd = [BFS.executable] + ['-f','USA-road-d.USA.gr']

#Single-Source Shortest Paths
SSSP=LiveProcess()
SSSP.executable = spec_dir + 'SSSP' + '/exe/' + 'sssp'
SSSP.cmd = [SSSP.executable] + ['-f','USA-road-d.USA.gr']

#PageRank
PR=LiveProcess()
PR.executable = spec_dir + 'PR' + '/exe/' + 'pr'
PR.cmd = [PR.executable] + ['-f','USA-road-d.USA.gr']

#Connected Components
CC=LiveProcess()
CC.executable = spec_dir + 'CC' + '/exe/' + 'cc'
CC.cmd = [CC.executable] + ['-f','USA-road-d.USA.gr']

#Betweenness Centrality
BC=LiveProcess()
BC.executable = spec_dir + 'BC' + '/exe/' + 'bc'
BC.cmd = [BC.executable] + ['-f','USA-road-d.USA.gr']

#Triangle Counting
TC=LiveProcess()
TC.executable = spec_dir + 'TC' + '/exe/' + 'tc'
TC.cmd = [TC.executable] + ['-f','USA-road-d.USA.gr']
