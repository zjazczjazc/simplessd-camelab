This is SimpleSSD-gem5SE source code.
Usage example:
./build/ARM/gem5.opt --debug-flags=HIL,FTLOut,PAL2,GLOBALCONFIG --debug-file=SimpleSSD.log ./configs/example/se_simpleSSD.py -c tests/test-progs/hello/bin/arm/linux/hello --alloc-size=16MB --dealloc-high=16 --dealloc-low=8 --SSD-param=1 --SSD-Config=src/mem/sample_cfg/rev_ch16.cfg
