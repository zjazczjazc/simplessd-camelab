#include "mem/GlobalConfig.h"
#include "mem/ConfigReader.h"

GlobalConfig::GlobalConfig(ConfigReader * cr){


	NANDType = cr->ReadInt32("NANDType",NAND_TLC),
	NumChannel = cr->ReadInt32("NumChannel",1),
	NumPackage = cr->ReadInt32("NumPackage",1),
	NumDie = cr->ReadInt32("NumDie",1),
	NumPlane = cr->ReadInt32("NumPlane",2),
	NumBlock = cr->ReadInt32("NumBlock",1368),
	NumPage = cr->ReadInt32("NumPage",384),
	SizePage = cr->ReadInt32("SizePage",8192),
	DMAMHz =  cr->ReadInt32("DMAMhz",50),
	EnableDMAPreemption = cr->ReadInt32("DMAPreemption", 1),

	FTLOP = cr->ReadFloat("FTLOP", 0.25);
	FTLGCThreshold = cr->ReadFloat("FTLGCThreshold", 0.07); 
	FTLMapN = cr->ReadInt32("FTLMapN", 32);
    FTLMapK = cr->ReadInt32("FTLMapK", 32);

	FTLEraseCycle = cr->ReadInt32("FTLEraseCycle", 100000); 
	SuperblockDegree = cr->ReadInt32("SuperblockDegree", NumChannel*NumPackage*NumDie);
	Warmup = cr->ReadFloat("Warmup", 1.0); 



	OriginalSizes[ADDR_CHANNEL] = NumChannel;
	OriginalSizes[ADDR_PACKAGE] = NumPackage;
	OriginalSizes[ADDR_DIE]     = NumDie;
	OriginalSizes[ADDR_PLANE]   = NumPlane;
	OriginalSizes[ADDR_BLOCK]   = NumBlock;
	OriginalSizes[ADDR_PAGE]    = NumPage;
	OriginalSizes[6]            = 0; //Add remaining bits

	AddrRemap[cr->ReadInt32("AddrRemap_CHANNEL", 5)] = ADDR_CHANNEL;
	AddrRemap[cr->ReadInt32("AddrRemap_PACKAGE", 4)] = ADDR_PACKAGE;
	AddrRemap[cr->ReadInt32("AddrRemap_DIE", 3)] = ADDR_DIE;
	AddrRemap[cr->ReadInt32("AddrRemap_PLANE", 2)] = ADDR_PLANE;
	AddrRemap[cr->ReadInt32("AddrRemap_BLOCK", 1)] = ADDR_BLOCK;
	AddrRemap[cr->ReadInt32("AddrRemap_PAGE", 0)] = ADDR_PAGE;

	int superblock = SuperblockDegree;
	AddrSeq[0] = AddrRemap[0];
	AddrSeq[1] = AddrRemap[1];
	AddrSeq[2] = AddrRemap[2];
	AddrSeq[3] = AddrRemap[3];
	AddrSeq[4] = AddrRemap[4];
	AddrSeq[5] = AddrRemap[5];

	int offset = 0;
	while (superblock > 1){
		if (superblock / OriginalSizes[AddrRemap[5-offset]] == 0){ //superblock is not aligned
			OriginalSizes[6] = OriginalSizes[AddrRemap[5-offset]] / superblock;
			AddrSeq[6] = offset;
			OriginalSizes[AddrRemap[5-offset]] = superblock;
			superblock = 0;
		}
		else superblock = superblock / OriginalSizes[AddrRemap[5-offset]];
		offset++;
	}


	//Search the page location
	for ( unsigned i = 0; i < 6; i++){
		if (AddrRemap[i] == ADDR_PAGE){
			if (i <= (5 - offset)){ //revise address remap, if super block is
									// inconsistent with input address remap info
				for (unsigned j = i; j < (5 - offset); j++ ){
					AddrRemap[j] = AddrRemap[j+1];
				}
				AddrRemap[5-offset] = ADDR_PAGE;
			}
			else{ //if address remap is not revised
				if (OriginalSizes[6] != 0){
					OriginalSizes[AddrRemap[5-AddrSeq[6]]] *= OriginalSizes[6];
					OriginalSizes[6] = 0;
				}
			}
			break;
		}
	}
	for (unsigned i = 0; i < 6; i++){
		AddrSeq[i] = AddrRemap[i];
	}

  PrintInfo();

}

void GlobalConfig::PrintInfo()
{
    //Use DPRINTF here - ALL of these
    printf("PAL: [ Configuration ]\n");
    printf("PAL: DMAPreemption=%d\n", EnableDMAPreemption);
    printf("PAL: plane count = %llu planes\n", GetTotalNumPlane() ); //cout<<"plane count = "<< GetTotalNumPlane()<<" planes\n";
    printf("PAL: block count = %llu blocks\n", GetTotalNumBlock() ); //cout<<"block count = "<< GetTotalNumBlock()<<" blocks\n";
    printf("PAL: page count = %llu pages\n", GetTotalNumPage() ); //cout<<"page count = "<< GetTotalNumPage()<<" pages\n";
    printf("PAL: size = %llu Byte\n", GetTotalSizeSSD() ); //cout<<"size = "<< GetTotalSizeSSD()<<" Byte\n";
    printf("PAL: size = %llu MByte\n", GetTotalSizeSSD()/(MBYTE) ); //cout<<"size = "<< GetTotalSizeSSD()/(MBYTE)<<" MByte\n";
    printf("PAL: size = %llu GByte\n", GetTotalSizeSSD()/(GBYTE) ); //cout<<"size = "<< GetTotalSizeSSD()/(GBYTE)<<" GByte\n";
    printf("PAL: AddrSeq:\n");
    printf("PAL: %8s | %8s | %8s | %8s | %8s | %8s\n", ADDR_STRINFO[AddrSeq[0]], ADDR_STRINFO[AddrSeq[1]], ADDR_STRINFO[AddrSeq[2]], ADDR_STRINFO[AddrSeq[3]], ADDR_STRINFO[AddrSeq[4]], ADDR_STRINFO[AddrSeq[5]]);
    
    printf("PAL: %8u | %8u | %8u | %8u | %8u | %8u\n", OriginalSizes[AddrSeq[0]], OriginalSizes[AddrSeq[1]], OriginalSizes[AddrSeq[2]], OriginalSizes[AddrSeq[3]], OriginalSizes[AddrSeq[4]], OriginalSizes[AddrSeq[5]]);

    switch (NANDType)
    {
        default:
        case NAND_TLC: printf("NANDType = TLC (REAL_TLC_PAGE8K)\n"); break;
        case NAND_MLC: printf("NANDType = MLC (ASSUMED_MLC)\n");     break;
        case NAND_SLC: printf("NANDType = SLC (ASSUMED_SLC)\n");     break;
    }
	
    printf("NAND DMA Speed = %u MHz, DMA Pagesize = %u Bytes\n", DMAMHz, SizePage);
}

uint64 GlobalConfig::GetTotalSizeSSD()
{
    return GetTotalNumPage() * (uint64)SizePage;
}

uint64 GlobalConfig::GetTotalNumPage()
{
    return GetTotalNumBlock() * (uint64)NumPage;
}

uint64 GlobalConfig::GetTotalNumBlock()
{
    return GetTotalNumPlane() * (uint64)NumBlock;
}

uint64 GlobalConfig::GetTotalNumDie()
{
    return (uint64)NumChannel * (uint64)NumPackage * (uint64)NumDie;
}

uint64 GlobalConfig::GetTotalNumPlane()
{
    return (uint64)NumChannel * (uint64)NumPackage * (uint64)NumDie * (uint64)NumPlane;
}
