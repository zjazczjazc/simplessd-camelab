
#ifndef __FTL_HH__
#define __FTL_HH__

#include <string>
#include <vector>
#include <map>

#include "arch/isa_traits.hh"
#include "base/types.hh"
#include "mem/hil.hh"

class HIL;

class FTL
{
protected:
public:
  HIL *hil;

  FTL();
  
  void readTransaction(Addr lpn, int pgNo, Tick TransTick); 
  void writeTransaction(Addr lpn, int pgNo, Tick TransTick); 
  void setHIL(HIL *h){
    hil = h;
  }

  ~FTL();

};


#endif // __FTL_HH__
