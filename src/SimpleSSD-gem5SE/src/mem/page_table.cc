/*
 * Copyright (c) 2014 Advanced Micro Devices, Inc.
 * Copyright (c) 2003 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Steve Reinhardt
 *          Ron Dreslinski
 *          Ali Saidi
 */

/**
 * @file
 * Definitions of functional page table.
 */
#include <fstream>
#include <map>
#include <memory>
#include <string>

#include "base/bitfield.hh"
#include "base/intmath.hh"
#include "base/trace.hh"
#include "config/the_isa.hh"
#include "debug/MMU.hh"
#include "debug/Dirty.hh"
#include "mem/page_table.hh"
//#include "mem/hil.hh"
//#include "mem/ftl.hh"
#include "sim/faults.hh"
#include "sim/sim_object.hh"

using namespace std;
using namespace TheISA;

FuncPageTable::FuncPageTable(const std::string &__name,
                             uint64_t _pid, Addr _pageSize, int SSDenable, string SSDConfig)
  : PageTableBase(__name, _pid, _pageSize, SSDenable, SSDConfig)
{

  hil = new HIL(0, SSDenable, SSDConfig);

  rdDl     = 1;
  wrtDl    = 1;
  rdBytes  = 0;
  wrtBytes = 0;
  wrtPg    = 0;
  rdPg     = 0;
  swpPg    = 0;
  rdBd     = 0;
  wrtBd    = 0;
  rdCount = 0;
  wrtCount = 0;
  SSDEnable = SSDenable;
}



FuncPageTable::~FuncPageTable()
{

}



void
FuncPageTable::setParam(Addr PmaxAllocSize, Addr PdeallocStart, Addr PdeallocStop, uint64_t PdeallocHigh, uint64_t PdeallocLow, int SSDparam){
    maxAllocSize = PmaxAllocSize;
    deallocStart = pageAlign(PdeallocStart);
    deallocStop = pageAlign(PdeallocStop);
    deallocHigh = PdeallocHigh * pageSize;
    deallocLow = PdeallocLow * pageSize;
    SwapCounter = 0;
    CurrSize = 0;
    hil->setSSD(SSDparam);
}

void
FuncPageTable::map(Addr vaddr, Addr paddr, int64_t size, Addr allocStart, uint64_t flags)
{
    bool clobber = flags & Clobber;
    // starting address must be page aligned
    assert(pageOffset(vaddr) == 0);
    DPRINTF(MMU, "Allocating Page: %#x-%#x\n", vaddr, vaddr+ size);

    for (; size > 0; size -= pageSize, vaddr += pageSize, paddr += pageSize) {
        if (!clobber && (pTable.find(vaddr) != pTable.end())) {
            // already mapped
            fatal("FuncPageTable::allocate: addr 0x%x already mapped", vaddr);
        }
	
        pTable[vaddr] = TheISA::TlbEntry(pid, vaddr, paddr,
                                         flags & Uncacheable,
                                         flags & ReadOnly);
        eraseCacheEntry(vaddr);
        updateCache(vaddr, pTable[vaddr]);
    }
}

void
FuncPageTable::changeFlag(Addr paddr, int size, bool Alloc)
{
    std::map<Addr, bool>::iterator iHigh;
    std::map<Addr, bool>::iterator iLow;
    std::map<Addr, bool>::iterator iFlag;
    std::map<Addr, bool>::iterator iDirty;
    std::map<Addr, Tick>::iterator iDelay;
    Addr tempAddr;
    Addr allocStart = paddr;
    Addr High = allocStart & (~(deallocHigh-1));
    Addr Low = High + deallocLow;
    int sizeTot = size*pageSize;
    int sizeDirty = 0;
    std::map<Addr, Tick>::iterator iVict;
    for (; sizeTot > 0; sizeTot -= pageSize, paddr += pageSize) {
      iFlag = pMapFlag.find(paddr);
      if (iFlag == pMapFlag.end())
        panic("Page Table error!\n");
      if (iFlag->second == Alloc){
	panic("Page Table Unexpected operation at Address %#x  of block %#x AllocFlag is %d Size %d CurrentSize: %lx (%lx-%lx)\n", paddr, allocStart, Alloc, size, CurrSize, deallocStart, deallocStop);
      }
      iFlag->second = Alloc;
    } 
    if(Alloc){
      hil->SSDoperation(allocStart, size, curTick(), Alloc);
      rdPg += size;
      rdBytes += (size * pageSize);
      rdCount++;
      iDelay = hil->dlMap.find(allocStart);
      if (iDelay == hil->dlMap.end())
	panic("Error: Delay Fault\n");
      if((rdDl == 1) && ( iDelay->second > 0))
	rdDl = iDelay->second;
      else 
	rdDl += iDelay->second;
    } else {
      DPRINTF(Dirty,"HIL: Write operation 0x%lx size of %d\n", allocStart, size);
      sizeTot = size*pageSize;
      for (tempAddr = allocStart; tempAddr < (allocStart + sizeTot); tempAddr += pageSize) {
	iDirty = pMapDirty.find(tempAddr);
	if(iDirty == pMapDirty.end())
	  panic("1Page Table error!!\n");
	DPRINTF(Dirty,"AllocStart 0x%lx TempAddr 0x%lx @ %d\n", allocStart, tempAddr, iDirty->second ? 1 : 0);	  
	if(iDirty->second) {
	  sizeDirty++;
	  iDirty->second = false;
	}
      }
      
      DPRINTF(Dirty,"HIL: Actual Write operation 0x%lx size of %d\n", allocStart, sizeDirty);
      if(sizeDirty != 0){
	hil->SSDoperation(allocStart, sizeDirty, curTick(), Alloc);
	wrtPg += sizeDirty;
	swpPg += size;
	wrtCount++;
	wrtBytes += (sizeDirty * pageSize);
	iDelay = hil->dlMap.find(allocStart);
	if (iDelay == hil->dlMap.end())
	  panic("Error: Delay Fault\n");
      if((wrtDl == 1) && ( iDelay->second > 0))
	wrtDl = iDelay->second;
      else 
	wrtDl += iDelay->second;
      }
    }
    if(Alloc){ 
     sizeTot = size*pageSize;
     if((allocStart == High) && (sizeTot == deallocHigh)){
       iVict = pMapLRUH.find(High);
       if(iVict == pMapLRUH.end()){
	 pMapLRUH.insert(std::pair<Addr, Tick>(High, curTick()));
       } else {
	 iVict->second = curTick();
       }
     }
     if (allocStart == High) {
       iVict = pMapLRUL.find(High);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(High, curTick()));
       } else {
	 iVict->second = curTick();
       }
     }

     if ((allocStart+sizeTot) == (High+deallocHigh)){
       iVict = pMapLRUL.find(Low);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(Low, curTick()));
       } else {
	 iVict->second = curTick();
       }
     } 
    }
    if(Alloc){
      CurrSize = CurrSize + (size*pageSize);
      DPRINTF(MMU, "Read %#x %d\n",allocStart, (size*8));
    } else {
      CurrSize = CurrSize - (size*pageSize);
      DPRINTF(MMU, "Write %#x %d\n",allocStart, (size*8));
    }
}

bool
FuncPageTable::isAlloc(Addr paddr)
{
    Addr High = paddr & (~(deallocHigh-1)); 
    Addr Low =  paddr & (~(deallocLow-1));
    std::map<Addr, bool>::iterator iKey = pMapFlag.find(paddr);    
    std::map<Addr, bool>::iterator iHigh = pMapFlag.find(High+deallocHigh);
    std::map<Addr, bool>::iterator iHighBeg = pMapFlag.find(High);
    std::map<Addr, bool>::iterator iHigh2 = pMapFlag.find(Low + deallocLow);
    std::map<Addr, bool>::iterator iHigh2Beg = pMapFlag.find(Low);
    if(iKey == pMapFlag.end()){
       panic("Error: Flag Segmentation Fault\n");
    }
    if(iKey->second ){
      std::map<Addr, Tick>::iterator iVict = pMapLRUH.find(High);
      if(iVict == pMapLRUH.end()  && iHigh->second && iHighBeg->second){
	pMapLRUH.insert(std::pair<Addr, Tick>(High, curTick()));
      } else {
	iVict->second = curTick();
      }
      iVict = pMapLRUL.find(Low);
      if(iVict == pMapLRUL.end() && iHigh2->second && iHigh2Beg->second){
	pMapLRUL.insert(std::pair<Addr, Tick>(Low, curTick()));
      } else {
	iVict->second = curTick();
      }
    }
    return iKey->second;
}

bool
FuncPageTable::needDealloc()
{
  if ((CurrSize > deallocStop) && (curTick() > 100000000))
    return true;
  else
    return false;
}

uint64_t
FuncPageTable::getWrtPg()
{
  return wrtPg;
}


uint64_t
FuncPageTable::getSwpPg()
{
  return swpPg;
}


uint64_t
FuncPageTable::getRdPg()
{
  return rdPg;
}


double
FuncPageTable::getWrtBd()
{
  return ((976562500.0 * ((double) wrtBytes)) / (1024.0 * ((double) wrtDl)));
}


double
FuncPageTable::getRdBd()
{
  return ((976562500.0 * ((double) rdBytes)) / (1024.0 * ((double) rdDl)));
}


double
FuncPageTable::getWrtBdTotTm()
{
  if(curTick() == 0)
    return 0.0;
  else
    return ((976562500.0 * ((double) wrtBytes)) / (1024.0 * ((double) curTick())));
}


double
FuncPageTable::getRdBdTotTm()
{
  if(curTick() == 0)
    return 0.0;
  else
    return ((976562500.0 * ((double) rdBytes)) / (1024.0 * ((double) curTick())));
}


double
FuncPageTable::getWrtBdWOIdle()
{
  return ((976562500.0 * ((double) wrtBytes)) / (1024.0 * ((double) (rdDl + wrtDl))));
}


double
FuncPageTable::getRdBdWOIdle()
{
  return ((976562500.0 * ((double) rdBytes)) / (1024.0 * ((double) (rdDl + wrtDl))));
}

uint64_t
FuncPageTable::getRdCount()
{
  return rdCount;
}

uint64_t
FuncPageTable::getWrtCount()
{
  return wrtCount;
}

void
FuncPageTable::setDirty(Addr paddr)
{
  paddr = pageAlign(paddr);
  std::map<Addr, bool>::iterator iKey  = pMapDirty.find(paddr);
  if(iKey == pMapDirty.end()){
       panic("Error: Not DirtyFlag Segmentation Fault\n");
  }
  DPRINTF(Dirty,"SetDirty @ 0x%lx\n", paddr);
  iKey->second = true;
}

void
FuncPageTable::setAllocSize(Addr allocStart, int size)
{
     Addr Low = allocStart & (~(deallocLow-1));
     Addr paddr = allocStart;
     Tick setTick = curTick();
     Addr High = allocStart & (~(deallocHigh-1)); 
     std::map<Addr, bool>::iterator iHigh = pMapFlag.find(High);
     std::map<Addr, bool>::iterator iLow = pMapFlag.find(Low);
     std::map<Addr, Tick>::iterator iVict;
     std::map<Addr, Tick>::iterator iDelay;

     hil->SSDoperation(allocStart, size, curTick(), true);
     rdPg += size;
     rdBytes += size * pageSize;
     rdCount++;
     iDelay = hil->dlMap.find(allocStart);
     if (iDelay == hil->dlMap.end())
       panic("Error: Delay Fault\n");

      if((rdDl == 1) && ( iDelay->second > 0))
	rdDl = iDelay->second;
      else 
	rdDl += iDelay->second;
     pMapSize.insert(std::pair<Addr, int>(allocStart, size));
     DPRINTF(MMU, "Read %#x %d\n", allocStart, (size*8));

     size = size*pageSize;

     bool isHigh = ((iHigh->second  && iLow->second) || (allocStart == High)) && ((High + deallocHigh)<=(allocStart+size));
     bool isLow  = ((iLow->second) || (allocStart == Low)) && ((Low + deallocLow)<=(allocStart+size)); 
     if(isHigh){
       iVict = pMapLRUH.find(High);
       if(iVict == pMapLRUH.end()){
	 pMapLRUH.insert(std::pair<Addr, Tick>(High, setTick));
       } else {
	 iVict->second = setTick;
       }
       iVict = pMapLRUL.find(High);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(High, setTick));
       } else {
	 iVict->second = setTick;
       }
       Low = High + deallocLow;
       iVict = pMapLRUL.find(Low);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(Low, setTick));
       } else {
	 iVict->second = setTick;
       }
     } else if (isLow){
       iVict = pMapLRUL.find(Low);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(Low, setTick));
       } else {
	 iVict->second = setTick;
       }
     }
     High = High + deallocHigh;

     for(; ((High + deallocHigh)<=(allocStart+size)) ; High+=deallocHigh ){

       iVict = pMapLRUH.find(High);
       if(iVict == pMapLRUH.end()){
	 pMapLRUH.insert(std::pair<Addr, Tick>(High, setTick));
       } else {
	 iVict->second = setTick;
       }
       iVict = pMapLRUL.find(High);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(High, setTick));
       } else {
	 iVict->second = setTick;
       }
       Low = High + deallocLow;
       iVict = pMapLRUL.find(Low);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(Low, setTick));
       } else {
	 iVict->second = setTick;
       }
      
     }
     if((High+deallocLow)<= (allocStart + size)){
       iVict = pMapLRUL.find(High);
       if(iVict == pMapLRUL.end()){
	 pMapLRUL.insert(std::pair<Addr, Tick>(High, setTick));
       } else {
	 iVict->second = setTick;
       }
     }

     for (; size > 0; size -= pageSize, paddr += pageSize) {
       CurrSize = CurrSize + (pageSize);
       pMapKey.insert(std::pair<Addr, Addr>(paddr, allocStart));
       pMapFlag.insert(std::pair<Addr, bool>(paddr, true));
       pMapDirty.insert(std::pair<Addr, bool>(paddr, false));
     }
}


Tick
FuncPageTable::chkDealloc()
{
  int size;
  Addr deallocAddr;
  if(CurrSize > deallocStart){
    SwapCounter++;
    deallocAddr =  getVictim(deallocHigh);
    if(deallocAddr == 1)
      return 0;
    std::map<Addr, bool>::iterator iKey = pMapFlag.find(deallocAddr + deallocLow);
    if(iKey->second)
      size = deallocHigh / pageSize;
    else
      size = deallocHigh / (2 * pageSize);
  } else if (CurrSize > deallocStop) { 
    SwapCounter = 0;
    deallocAddr =  getVictim(deallocLow);
    if(deallocAddr == 1)
      return 0;
    size = deallocLow / pageSize;

  } else {
    SwapCounter = 0;
    return 0;
  } 
  changeFlag(deallocAddr, size, false);
  return getLatency(deallocAddr);

}

Addr
FuncPageTable::getVictim(uint64_t size)
{
  int BSize = size / pageSize;
  std::map<Addr, Tick>::iterator iVict;
  std::map<Addr, bool>::iterator iDrt, iDrtTemp;
  Addr tempAddr;
  std::map<Addr, Addr>::iterator iTemp;
  std::map<Addr, int>::iterator iSize;
  Addr victAddr = 1;
  Tick tempTick = curTick() - 10000000;
  if(size > deallocLow){
    for(iVict = pMapLRUH.begin(); iVict != pMapLRUH.end(); iVict++) {
      if(iVict->second > 0){
	if((iVict->second <= tempTick) && (iVict->second != curTick())&& (pMapKey.find((iVict->first + size)) != pMapKey.end())){ 
	  victAddr = iVict->first;
	  tempTick = iVict->second;
	}
      }
    }

    if(victAddr != 1) {
      pMapLRUH.erase(victAddr);
      pMapLRUL.erase(victAddr);
      pMapLRUL.erase(victAddr + (BSize * pageSize / 2));
    }
  } else {
    for(iVict = pMapLRUL.begin(); iVict != pMapLRUL.end(); iVict++) {
      if(iVict->second > 0){
	if((iVict->second <= tempTick)  && (iVict->second != curTick()) && (pMapKey.find((iVict->first + size)) != pMapKey.end())){
	  victAddr = iVict->first;
	  tempTick = iVict->second;
	}
      }
    }
  

    if(victAddr != 1){
      pMapLRUH.erase(victAddr & (~ (deallocHigh-1)));
      pMapLRUL.erase(victAddr); 
    }
  }
  if (victAddr != 1){
    /////////////////////////////////Delay Flag Fault
    std::map<Addr, Addr>::iterator iKeyVict = pMapKey.find(victAddr);
    if(iKeyVict == pMapKey.end())
      panic("Error: something wrong to find address 0x%x at %lu\n", victAddr,tempTick);
    Addr changeAddr = iKeyVict->second; 
    if (victAddr != changeAddr){
    std::map<Addr, Tick>::iterator iDelay     = hil->delayMap.find(victAddr);
    if(iDelay == hil->delayMap.end())
      hil->delayMap.insert(std::pair<Addr, Tick>(victAddr, 0));
    }
    /////////////////////////////////Delay Flag Fault
    for(tempAddr=(victAddr); tempAddr < victAddr+size; tempAddr+=pageSize){
      iTemp = pMapKey.find(tempAddr);
      if(iTemp == pMapKey.end())
	panic("Page Align in getting Victim at Address %lx %lx %lx %lx\n", tempAddr, victAddr, size, CurrSize);
      iTemp->second = victAddr;
    }
    iSize = pMapSize.find(victAddr);
    if(iSize == pMapSize.end()){
      pMapSize.insert(std::pair<Addr, int>(victAddr, BSize));
    } else {
      iSize->second = BSize; 
    }
  }
  
  return victAddr;
}

Tick
FuncPageTable::getLatency(Addr address)
{
    std::map<Addr, Addr>::iterator iKey = pMapKey.find(address);
    if(iKey == pMapKey.end())
       panic("Error: something wrong to find address2\n");
    Addr changeAddr = iKey->second; 
    std::map<Addr, Tick>::iterator iDelay = hil->delayMap.find(changeAddr);
    if(iDelay == hil->delayMap.end()){
       printf("the address is %lu\n",changeAddr);
       panic("Error: Flag Delay Fault\n");
    }
    Tick delay = iDelay->second;
    iDelay->second = 0; 
    return delay;
  
}


void
FuncPageTable::realloc(Addr address)
{
    std::map<Addr, Tick>::iterator iVict;
    std::map<Addr, Addr>::iterator iKey = pMapKey.find(address);
    Addr reallocAddr = iKey->second; 
    std::map<Addr, int>::iterator iSize = pMapSize.find(reallocAddr);
    if((iSize->second * pageSize) == deallocHigh) {
      iVict = pMapLRUL.find(reallocAddr + deallocLow);
      if(iVict != pMapLRUL.end())
	iSize->second = iSize->second/2;
    }
	
    changeFlag(reallocAddr, iSize->second, true);
}


void
FuncPageTable::printStats()
{
  hil->printStats();
}

void
FuncPageTable::get_parameter(enum layer_type layer, struct output_result &output)
{
  hil->get_parameter(layer,output);
}

int
FuncPageTable::getSSDFlag()
{
  return SSDEnable;
}

void
FuncPageTable::remap(Addr vaddr, int64_t size, Addr new_vaddr)
{
    assert(pageOffset(vaddr) == 0);
    assert(pageOffset(new_vaddr) == 0);

    DPRINTF(MMU, "moving pages from vaddr %08p to %08p, size = %d\n", vaddr, new_vaddr, size);

    for (; size > 0;
         size -= pageSize, vaddr += pageSize, new_vaddr += pageSize)
    {
        assert(pTable.find(vaddr) != pTable.end());

        pTable[new_vaddr] = pTable[vaddr];
        pTable.erase(vaddr);
        eraseCacheEntry(vaddr);
        pTable[new_vaddr].updateVaddr(new_vaddr);
        updateCache(new_vaddr, pTable[new_vaddr]);
    }
}

void
FuncPageTable::unmap(Addr vaddr, int64_t size)
{
    assert(pageOffset(vaddr) == 0);

    //DPRINTF(MMU, "Unmapping page: %#x-%#x\n", vaddr, vaddr+ size);

    for (; size > 0; size -= pageSize, vaddr += pageSize) {
        assert(pTable.find(vaddr) != pTable.end());
        pTable.erase(vaddr);
        eraseCacheEntry(vaddr);
    }

}

bool
FuncPageTable::isUnmapped(Addr vaddr, int64_t size)
{
    // starting address must be page aligned
    assert(pageOffset(vaddr) == 0);

    for (; size > 0; size -= pageSize, vaddr += pageSize) {
        if (pTable.find(vaddr) != pTable.end()) {
            return false;
        }
    }

    return true;
}

bool
FuncPageTable::lookup(Addr vaddr, TheISA::TlbEntry &entry)
{
    Addr page_addr = pageAlign(vaddr);

    if (pTableCache[0].valid && pTableCache[0].vaddr == page_addr) {
        entry = pTableCache[0].entry;
        return true;
    }
    if (pTableCache[1].valid && pTableCache[1].vaddr == page_addr) {
        entry = pTableCache[1].entry;
        return true;
    }
    if (pTableCache[2].valid && pTableCache[2].vaddr == page_addr) {
        entry = pTableCache[2].entry;
        return true;
    }

    PTableItr iter = pTable.find(page_addr);

    if (iter == pTable.end()) {
        return false;
    }

    updateCache(page_addr, iter->second);
    entry = iter->second;
    return true;
}

bool
PageTableBase::translate(Addr vaddr, Addr &paddr)
{
    TheISA::TlbEntry entry;
    if (!lookup(vaddr, entry)) {
        //DPRINTF(MMU, "Couldn't Translate: %#x\n", vaddr);
        return false;
    }
    paddr = pageOffset(vaddr) + entry.pageStart();
    //DPRINTF(MMU, "Translating: %#x->%#x\n", vaddr, paddr);
    return true;
}

Fault
PageTableBase::translate(RequestPtr req)
{
    Addr paddr;
    assert(pageAlign(req->getVaddr() + req->getSize() - 1)
           == pageAlign(req->getVaddr()));
    if (!translate(req->getVaddr(), paddr)) {
        return Fault(new GenericPageTableFault(req->getVaddr()));
    }
    req->setPaddr(paddr);
    if ((paddr & (pageSize - 1)) + req->getSize() > pageSize) {
        panic("Request spans page boundaries!\n");
        return NoFault;
    }
    return NoFault;
}

void
FuncPageTable::serialize(CheckpointOut &cp) const
{
    paramOut(cp, "ptable.size", pTable.size());

    PTable::size_type count = 0;
    for (auto &pte : pTable) {
        ScopedCheckpointSection sec(cp, csprintf("Entry%d", count++));

        paramOut(cp, "vaddr", pte.first);
        pte.second.serialize(cp);
    }
    assert(count == pTable.size());
}

void
FuncPageTable::unserialize(CheckpointIn &cp)
{
    int count;
    paramIn(cp, "ptable.size", count);

    for (int i = 0; i < count; ++i) {
        ScopedCheckpointSection sec(cp, csprintf("Entry%d", i));

        std::unique_ptr<TheISA::TlbEntry> entry;
        Addr vaddr;

        paramIn(cp, "vaddr", vaddr);
        entry.reset(new TheISA::TlbEntry());
        entry->unserialize(cp);

        pTable[vaddr] = *entry;
    }

}

