#ifndef __ssdsim_h__
#define __ssdsim_h__

#include "mem/SimpleSSD_types.h"

#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <fstream>
using namespace std;

#include "mem/GlobalConfig.h"
#include "mem/ConfigReader.h"


#include "mem/Latency.h"
#include "mem/LatencySLC.h"
#include "mem/LatencyMLC.h"
#include "mem/LatencyTLC.h"

#include "mem/Simulator.h"
#include "mem/PALStatistics.h"

#include "mem/PAL2.h"

#include "mem/ftl.hh"



#endif //__ssdsims_h__
