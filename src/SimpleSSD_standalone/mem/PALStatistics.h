#ifndef __PALStatistics_h__
#define __PALStatistics_h__

#include "mem/SimpleSSD_types.h"

#include "mem/GlobalConfig.h"
#include "mem/Latency.h"
#include "mem/PAL2_TimeSlot.h"
#include "mem/ftl.hh"
#include "mem/ftl_commandqueue.hh"

#include "base/types.hh"


#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <fstream>
using namespace std;


class Command;
#define OPER_ALL (OPER_NUM+1)
#define PAGE_ALL (PAGE_NUM+1)

class PALStatistics
{
    public:
        enum
        {
            TICK_DMA0_CHANNEL_CONFLICT = TICK_NUM,
            TICK_DMA0_PLANE_CONFLICT,
            TICK_DMA1_CONFLICT,
            TICK_DMA0_SUSPEND,
            TICK_DMA1_SUSPEND,
            TICK_PROC,
            TICK_FULL,
            TICK_STAT_NUM
        };



        PALStatistics();

        ~PALStatistics();


        void AddLatency(Task* task);


        uint64 sim_start_time_ps;
        uint64 LastTick;
        void UpdateLastTick(uint64 tick);
        uint64 GetLastTick();
        void AddLatency(Command* CMD, CPDPBP* CPD, uint32 dieIdx, TimeSlot* DMA0, TimeSlot* MEM, TimeSlot* DMA1, uint8 confType, uint64 confLength );
        void MergeSnapshot();
        uint64 ExactBusyTime, SampledExactBusyTime;
        uint64 OpBusyTime[3], LastOpBusyTime[3]; // 0: Read, 1: Write, 2: Erase;
        uint64 LastExactBusyTime;
        uint64 LastExecutionTime;

        void PrintStats(uint64 sim_time_ps);
        void ResetStats();
        void PrintFinalStats(uint64 sim_time_ps);

        ///////////////////////////////// polished stats
        class Counter
        {
          public:
            Counter();
            void init();
            void add();
            uint64 cnt;
        };

        class CounterOper
        {
          public:
            CounterOper();
            Counter cnts[OPER_ALL];
            void init();
            void add(uint32 oper);
            void printstat(const char* namestr);
        };

        CounterOper  PPN_requested_rwe;
        CounterOper  PPN_requested_pagetype[PAGE_ALL];
        CounterOper* PPN_requested_ch; //channels
        CounterOper* PPN_requested_die; //dies
        CounterOper  CF_DMA0_dma;
        CounterOper  CF_DMA0_mem;
        CounterOper  CF_DMA0_none;
        CounterOper  CF_DMA1_dma;
        CounterOper  CF_DMA1_none;

        class Value
        {
          public:
            Value();
            void init();
            void add(double val);
            void backup();
            void update();
            double avg();
            double legacy_avg();
            double sum, minval, maxval, cnt, sampled_sum, sampled_cnt, legacy_sum, legacy_cnt, legacy_minval, legacy_maxval;
        };

        class ValueOper
        {
          public:
            ValueOper();
            ValueOper(ValueOper *_ValueOper);
            Value vals[OPER_ALL];
            void init();
            void update();
            void add(uint32 oper, double val);
            void exclusive_add(uint32 oper, double val);
            void printstat(const char* namestr);
            void printstat_bandwidth(class ValueOper*, uint64 , uint64 ); //bandwidth excluding idle time
            void printstat_bandwidth_widle(class ValueOper*, uint64, uint64); //bandwidth including idle time
            void printstat_oper_bandwidth(class ValueOper*, uint64*, uint64*); // read/write/erase-only bandwidth
            void printstat_latency(const char* namestr);
            void printstat_iops(class ValueOper*, uint64 , uint64 );
            void printstat_iops_widle(class ValueOper*, uint64 , uint64 );
            void printstat_oper_iops(class ValueOper*, uint64* , uint64* );
        };

        ValueOper  Ticks_DMA0WAIT;
        ValueOper  Ticks_DMA0;
        ValueOper  Ticks_MEM;
        ValueOper  Ticks_DMA1WAIT;
        ValueOper  Ticks_DMA1;
        ValueOper  Ticks_Total; // Total = D0W+D0+M+D1W+D1
        std::map<uint64, ValueOper*> Ticks_Total_snapshot;
        ValueOper  Ticks_TotalOpti; //TotalOpti = D0+M+D1 --- exclude WAIT
        ValueOper* Ticks_Active_ch; //channels
        ValueOper* Ticks_Active_die; //dies
        ValueOper  Access_Capacity;
        std::map<uint64, ValueOper*> Access_Capacity_snapshot;
        ValueOper  Access_Bandwidth;
        ValueOper  Access_Bandwidth_widle;
        ValueOper  Access_Oper_Bandwidth;
        ValueOper  Access_Iops;
        ValueOper  Access_Iops_widle;
        ValueOper  Access_Oper_Iops;
        uint64 SampledTick;
        bool skip;
    private:
        void ClearStats();
        void InitStats();
};

#endif //__PALStatistics_h__
